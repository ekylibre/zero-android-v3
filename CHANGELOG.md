# Changelog

## 1.0.5 (_alpha_)// 2019-09-20
- Fix a crash because of only volume units for phytosanitary products. Now accepts volume and mass units.

## 0.9.4 (bêta) // 2018-06-25
- Improves harvest management, especially with global 

## 0.9.3 (bêta) // 2018-06-15
- Improves Oauth token refresh after long idle

## 0.9.2 (bêta) // 2018-06-14
- Corrects Oauth token refresh


## 0.9.1 (bêta) // 2018-06-12
- Token refresh
- Larger touch zones
- Synchronization in all ways
- Harvest creation
- More verifications before saving an intervention

## 0.9.0 (bêta) // 2018-06-05
First release on PlayStore