package com.ekylibre.android.utils;


import com.ekylibre.android.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateTools {

    public static final SimpleDateFormat STANDARD_DISPLAY = new SimpleDateFormat("dd/MM/yyyy", MainActivity.LOCALE);

    private static final SimpleDateFormat SIMPLE_DATE = new SimpleDateFormat("d MMM", MainActivity.LOCALE);
    private static final SimpleDateFormat DATE_WITH_YEAR = new SimpleDateFormat("d MMM yyyy", MainActivity.LOCALE);

    private static long MIDNIGHT = setMidnight();
    private static final int DAY = 86400000;
    private static long THIS_YEAR = setYear();

    public static String display(Date dt) {

        long date = dt.getTime();

        if (date >= MIDNIGHT && date <= MIDNIGHT + DAY)
            return "aujourd'hui";

        else if (date >= MIDNIGHT - DAY)
            return "hier";

        else if (date <= THIS_YEAR )
            return DATE_WITH_YEAR.format(dt);

        return SIMPLE_DATE.format(dt);
    }

    private static long setMidnight() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    private static long setYear() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }
}
